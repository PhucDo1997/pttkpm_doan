drop database if exists QLHS;
create database if not exists QLHS character set 'utf8' collate 'utf8_unicode_ci';
use QLHS;

CREATE TABLE HOCSINH
(
	MSHS varchar(15) not null,
	HOTEN nvarchar(50),
	GIOITINH nchar(3),
	NGAYSINH varchar(15),
	DIACHI nvarchar(100),
	EMAIL varchar(30),
	MALOP int,
	primary key(MSHS)
);
CREATE TABLE LOP
(
	MALOP int not null auto_increment,
	KHOI int,
	TEN char(10) unique,
	SISO int,
	primary key(MALOP)
);

CREATE TABLE DIEM15P
(
	MSHS varchar(15) not null,
	KHOA char(15),
	HK1TOAN float,
	HK1LY float,
	HK1HOA float,
	HK2TOAN float,
	HK2LY float,
	HK2HOA float,
	primary key(MSHS, KHOA)
);

CREATE TABLE DIEM1T
(
	MSHS varchar(15) not null,
	KHOA char(15),
	HK1TOAN float,
	HK1LY float,
	HK1HOA float,
	HK2TOAN float,
	HK2LY float,
	HK2HOA float,
	primary key(MSHS, KHOA)
);

CREATE TABLE DIEMCHK
(
	MSHS varchar(15) not null,
	KHOA char(15),
	HK1TOAN float,
	HK1LY float,
	HK1HOA float,
	HK2TOAN float,
	HK2LY float,
	HK2HOA float,
	primary key(MSHS, KHOA)
);

CREATE TABLE TAIKHOANHS
(
	ID int not null AUTO_INCREMENT,
	MSHS varchar(15) not null,
	MATKHAU char(100) not null,
	primary key(ID)
);

CREATE TABLE TAIKHOANGV
(
	TENTK varchar(50) not null,
	MATKHAU char(100) not null,
	primary key(TENTK)
);

ALTER TABLE DIEM15P
ADD CONSTRAINT FK_DIEM15P_HOCSINH
FOREIGN KEY (MSHS)
REFERENCES HOCSINH(MSHS);

ALTER TABLE DIEM1T
ADD CONSTRAINT FK_DIEM1T_HOCSINH
FOREIGN KEY (MSHS)
REFERENCES HOCSINH(MSHS);

ALTER TABLE DIEMCHK
ADD CONSTRAINT FK_DIEMCHK_HOCSINH
FOREIGN KEY (MSHS)
REFERENCES HOCSINH(MSHS);


ALTER TABLE TAIKHOANHS
ADD CONSTRAINT FK_TAIKHOANHS_HOCSINH
FOREIGN KEY (MSHS)
REFERENCES HOCSINH(MSHS);


ALTER TABLE HOCSINH
ADD CONSTRAINT FK_HOCSINH_LOP
FOREIGN KEY (MALOP)
REFERENCES LOP(MALOP);