﻿Các controller trong controller_thongtin:
- Add.php: tăng thêm 1 học sinh vào csdl
+ input: values{"MSHS", "HOTEN", "GIOITINH", "NGAYSINH", "DIACHI", "EMAIL"}
+ output:
	{"res":"true"}
	{"res":"false"}

- AddBangDiem.php: khởi tạo bảng điểm cho 1 học sinh (khởi tạo khi bắt đầu khóa mới)
+ input: values{"MSHS"}
+ output:
	{"res":"true"}
	{"res":"false"}

- AddToClass.php: thêm học sinh và 1 lớp
+ input: values{"MSHS","MALOP"}
+ output:
	{"res":"true"}
	{"res":"false"}

- Delete.php: xóa 1 học sinh
+ input: values{"MSHS"}
+ output:
	{"res":"true"}
	{"res":"false"}

- Login.php: Login
+ input: values{"USERNAME","PASSWORD"}
+ output:
	{"res":"true", "type":"admin"} || {"res":"false", "type":"admin"}
	{"res":"true", "type":"student"} || {"res":"false", "type":"student"}

- SearchID.php: tìm học sinh bằng MSHS
+ input: values{"MSHS"}
+ output:
	$hocsinh{"MSHS", "HOTEN", "GIOITINH", "NGAYSINH", "DIACHI", "EMAIL", "MALOP", "MALOP"}
	{"res":"false"}

- SearchTen.php: tìm các học sinh giống tên
+ input: values{"HOTEN"}
+ output:
	(array)$hocsinh{"MSHS", "HOTEN", "GIOITINH", "NGAYSINH", "DIACHI", "EMAIL", "MALOP"}[]
	vd:	[{"MSHS":"1","HOTEN":"a","GIOITINH":"nam","NGAYSINH":"1234","DIACHI":"q1","EMAIL":"abc@gmail.com","MALOP":"1"},
		{"MSHS":"10","HOTEN":"ka","GIOITINH":"nam","NGAYSINH":"1234","DIACHI":"q1","EMAIL":"abc@gmail.com","MALOP":"1"},
		{"MSHS":"11","HOTEN":"al","GIOITINH":"nam","NGAYSINH":"1234","DIACHI":"q1","EMAIL":"abc@gmail.com","MALOP":"2"}]
	{"res":"false"}

- Update.php: update thông tin học sinh trừ mã lớp
+ input: values{"MSHS", "HOTEN", "GIOITINH", "NGAYSINH", "DIACHI", "EMAIL"}
+ output:
	{"res":"true"}
	{"res":"false"}

- SearchMaLop.php: tìm các học sinh trong lớp theo mã lớp
+ input: values{"MALOP"}
+ output:
	(array)$hocsinh{"MSHS", "HOTEN", "GIOITINH", "NGAYSINH", "DIACHI", "EMAIL"}[]
	{"res":"false"}

- BaoCaoTongKetMon.php: xuất báo cáo tổng kết môn
+ input: values{"MON", "KHOA", "HOCKI"}
+ output:
	(array)$baocao{"MALOP", "KHOI", "TEN", "SISO", "SLD", "TYLE"}[]
	vd:	[{"MALOP":"1","KHOI":"12","TEN":"a1","SISO":"10","SLD":"1","TYLE":"10.0000"},
		{"MALOP":"2","KHOI":"12","TEN":"a2","SISO":"10","SLD":"0","TYLE":"0.0000"}]
	{"res":"false"}

- BaoCaoTongKetHocKi.php: xuất báo cáo tổng kết theo học kỳ
+ input: values{"KHOA", "HOCKI"}
+ output:
	(array)$baocao{"MALOP", "KHOI", "TEN", "SISO", "SLD", "TYLE"}[]
	vd:	[{"MALOP":"1","KHOI":"12","TEN":"a1","SISO":"10","SLD":"1","TYLE":"10.0000"},
		{"MALOP":"2","KHOI":"12","TEN":"a2","SISO":"10","SLD":"0","TYLE":"0.0000"}]
	{"res":"false"}

- UpdateAll.php: update tất cả thông tin học sinh
+ input: values{"MSHS", "HOTEN", "GIOITINH", "NGAYSINH", "DIACHI", "EMAIL", "MALOP"}
+ output:
	{"res":"true"}
	{"res":"false"}