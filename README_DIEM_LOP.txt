﻿ - ThemDiem15
	Thêm điểm 15p (thêm vào bảng diem15p)
	input: values{"MSHS", "KHOA", "MON", "HOCKI", "DIEM"}
	output:
		{"res":"true"} HOẶC
		{"res":"false"}

 - ThemDiem1t
	Thêm điểm 1 tiết (thêm vào bảng diem1t)
	input: values{"MSHS", "KHOA", "MON", "HOCKI", "DIEM"}
	output:
		{"res":"true"} HOẶC
		{"res":"false"}

 - ThemDiemCHK
	Thêm điểm cuối học kì (thêm vào bảng diemchk)
	input: values{"MSHS", "KHOA", "MON", "HOCKI", "DIEM"}
	output:
		{"res":"true"} HOẶC
		{"res":"false"}

 - TraCuuDiemAll
	Tra cứu tất cả điểm của 1 học sinh
	input: values{"MSHS"}
	output:
		{"ma":"val","khoa":"val","loaidiem":"val","loaidiem":"val"}
	Vd:{"mshs":"1","khoa":"2018","15pToanHk1":"9.9","15pLyHk1":"10","15pHoaHk1":"9.4","15pToanHk2":"5","15pLyHk2":"9.9","15pHoaHk2":"9.6","1tToanHk1":"9","1tLyHk1":"9.9","1tHoaHk1":"9.1","1tToanHk2":"9.8","1tLyHk2":"9.5","1tHoaHk2":"9.2","ChkToanHk1":"9.1","ChkLyHk1":"9.2","ChkHoaHk1":"9.3","ChkToanHk2":"9.7","ChkLyHk2":"9.6","ChkHoaHk2":"9.4"} 
	HOẶC
		{"res":"false", "err":"khong ton tai hoc sinh"}


 - TraCuuDiemTheoHK
	Tra cứu điểm theo học kì của 1 học sinh (tất cả các môn + khóa)
	input: values{"MSHS", "KHOA", "MON", "HOCKI", "DIEM"}
	output:
		{"ma":"val","khoa":"val","loaidiem":"val","loaidiem":"val"}
	Vd:{"mshs":"1","khoa":"2018","15pToanHK1":"9.9","15pLyHK1":"10","15pHoaHK1":"9.4","1tToanHK1":"9","1tLyHK1":"9.9","1tHoaHK1":"9.1","CHKToanHK1":"9.1","CHKLyHK1":"9.2","CHKHoaHK1":"9.3"}
	HOẶC
		{"res":"false", "err":"khong ton tai hoc sinh"}

 - TraCuuDiemTheoKhoa
	Tra cứu điểm theo khóa học của 1 học sinh (tất cả môn + học kì)
	input: values{"MSHS", "KHOA"}
	output:
		{"mshs":"1","khoa":"12","15pToanHk1":"9.9","15pLyHk1":"10","15pHoaHk1":"9.4","15pToanHk2":"5","15pLyHk2":"9.9","15pHoaHk2":"9.6","1tToanHk1":"9","1tLyHk1":"9.9","1tHoaHk1":"9.1","1tToanHk2":"9.8","1tLyHk2":"9.5","1tHoaHk2":"9.2","ChkToanHk1":"9.1","ChkLyHk1":"9.2","ChkHoaHk1":"9.3","ChkToanHk2":"9.7","ChkLyHk2":"9.6","ChkHoaHk2":"9.4"} 
	HOẶC
		{"res":"false", "err":"khong ton tai hoc sinh"}

 - TraCuuDiemTheoMon
	Tra cứu điểm theo 1 môn của 1 học sinh (tất cả khóa + học kì)
	input: values{"MSHS", "MON"}
	output:
		{"MSHS":"1","KHOA":"12","15ptoanHK1":"9.9","15ptoanHK2":"5","1ttoanHK1":"9","1ttoanHK2":"9.8","CHKtoanHK1":"9.1","CHKtoanHK2":"9.7"}
	HOẶC
		{"res":"false"}

 - UpdateDiem
	Chỉnh sửa điểm 1 học sinh
	input: values{"MSHS", "KHOA", "MON", "HOCKI", "DIEM15", "DIEM1T", "DIEMCHK"}
	output:
		{"res":"true"} HOẶC
		{"res":"false"}

 - XoaDiem
	Xóa điểm của 1 học sinh
	input: values{"MSHS", "KHOA", "MON", "HOCKI", LOAI"}
	output:
		{"res":"true"} HOẶC
		{"res":"false"}

===================================================

 - ChuyenLop
	Chuyển 1 học sinh từ lớp này đến lớp khác
	input: values{"MSHS", "MALOPCU", "MALOPMOI"}
	output:
		{"res":"true"} HOẶC
		{"res":"false"}

 - ThemHSVaoLopCoSan
	Lập danh sách lớp từ danh sách học sinh (có thể dùng thêm 1 học sinh).
	input: values{"MSHS", "DSMAHS"}
	output:
		{"res":"true"} HOẶC
		{"res":"false"}

 - TaoLop
	Tạo một lớp mới
	Tham số vào: Khối, tên lớp.input: values{"MSHS", "KHOA", "MON", "HOCKI", "DIEM"}
	output:
		{"res":"true"} HOẶC
		{"res":"false"}

 - TraCuuThongTinLop
	Tra cứu thông tin của 1 lớp
	input: values{"MALOP"}
	output:
		{"ten":"12a5","khoi":"12","siso":"33"} 
	HOẶC
		{"res":"false", "err":"khong ton tai lop"}

 - Xoa1HocSinhKhoiLop
	Xóa 1 học sinh ra khỏi 1 lớp
	input: values{"MSHS", "MALOP"}
	output:
		{"res":"true"} HOẶC
		{"res":"false"}

 - Xoa1Lop
	Xóa thông tin 1 lớp
	input: values{"MALOP"}
	output:
		{"res":"true"} HOẶC
		{"res":"false"}

 - TraCuuDanhSachHSLop
	Tra cứu danh sách học sinh của 1 lớp
	input: values{"MALOP"}
	output:
[{"MSHS":"2","HOTEN":"Phat1","GIOITINH":"Nam","NGAYSINH":"1997-09-23","DIACHI":"Cao Lo","EMAIL":"fals@gmail.com","MALOP":"1"},{"MSHS":"3","HOTEN":"sdf","GIOITINH":"Nu","NGAYSINH":"1997-09-23","DIACHI":"asf asd","EMAIL":"asdf@gmail.com","MALOP":"1"},{"MSHS":"4","HOTEN":"sat","GIOITINH":"Nam","NGAYSINH":"1997-09-23","DIACHI":"ash awe","EMAIL":"lsov@gmail.com","MALOP":"1"}] 
	HOẶC
		{"res":"false", "err":"khong ton tai lop"}

 - TraCuuKhoa
	Tra cứu các khóa đã học của 1 học sinh
	input: values{"MSHS"}
	output:
		["10","12","11","9","13"]
 	HOẶC
		{"res":"false", "err":"hoc sinh khong ton tai"}

- ThemDanhSachHS (chỉ dùng cho Module_Lop)
	Thêm tất cả thông tin của danh sách học sinh mới vào csdl
	Không có controller.

- LapDanhSachLopCSV
	Lập 1 lớp mới từ 1 danh sách học sinh mới.
	input: values{"KHOI", "TEN", "DSHOCSINH"}
	output:
		{"res":"true"} HOẶC
		{"res":"false"}

