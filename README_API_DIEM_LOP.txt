﻿ - ThemDiem15
	Thêm điểm 15p (thêm vào bảng diem15p)
	Tham số vào: Mã học sinh, khóa, tên môn, học kì, điểm 15p.
	Trả về true nếu thêm thành công, ngược lại false và cho biết lỗi (nếu được).

 - ThemDiem1t
	Thêm điểm 1 tiết (thêm vào bảng diem1t)
	Tham số vào là mảng có: Mã học sinh, khóa, tên môn, học kì, điểm 1t.
	Trả về true nếu thêm thành công, ngược lại false và cho biết lỗi (nếu được).

 - ThemDiemCHK
	Thêm điểm cuối học kì (thêm vào bảng diemchk)
	Tham số vào là mảng có: Mã học sinh, khóa, tên môn, học kì, điểm chk.
	Trả về true nếu thêm thành công, ngược lại false và cho biết lỗi (nếu được).

 - TraCuuDiemAll
	Tra cứu tất cả điểm của 1 học sinh
	Tham số vào: Mã học sinh
	Trả về file json chứa các trường điểm của học sinh.

 - TraCuuDiemTheoHK
	Tra cứu điểm theo học kì của 1 học sinh (tất cả các môn + khóa)
	Tham số vào: Mã học sinh, học kì
	Trả về file json chứa các trường điểm của học sinh.

 - TraCuuDiemTheoKhoa
	Tra cứu điểm theo khóa học của 1 học sinh (tất cả môn + học kì)
	Tham số vào: Mã học sinh, khóa
	Trả về file json chứa các trường điểm của học sinh.

 - TraCuuDiemTheoMon
	Tra cứu điểm theo 1 môn của 1 học sinh (tất cả khóa + học kì)
	Tham số vào: Mã học sinh, môn
	Trả về file json chứa các trường điểm của học sinh.

 - UpdateDiem
	Chỉnh sửa điểm 1 học sinh
	Tham số vào: Mã học sinh, khóa, tên môn, học kì, giá trị điểm muốn sửa, loại điểm sửa (15p - 1t - chk).
	Trả về true nếu thêm thành công, ngược lại false và cho biết lỗi (nếu được).

 - XoaDiem
	Xóa điểm của 1 học sinh
	Tham số vào: Mã học sinh, khóa, tên môn, học kì, loại điểm (15p - 1t - chk).
	Trả về true nếu thêm thành công, ngược lại false và cho biết lỗi (nếu được).

===================================================

 - ChuyenLop
	Chuyển 1 học sinh từ lớp này đến lớp khác
	Tham số vào: Mã học sinh, mã lớp cũ, mã lớp mới.
	Trả về true nếu thêm thành công, ngược lại false và cho biết lỗi (nếu được).

 - LapDanhSachLop
	Lập danh sách lớp từ danh sách học sinh (có thể dùng thêm 1 học sinh).
	Tham số vào là mảng có: Mã lớp, danh sách mã các học sinh cần thêm.
	Trả về true nếu thêm thành công, ngược lại false và cho biết lỗi (nếu được).

 - TaoLop
	Tạo một lớp mới
	Tham số vào: Khối, tên lớp.
	Trả về true nếu thêm thành công, ngược lại false và cho biết lỗi (nếu được).

 - TraCuuLop
	Tra cứu thông tin của 1 lớp
	Tham số vào: Mã lớp.
	Trả về file json chứa thông của lớp.

 - Xoa1HocSinhKhoiLop
	Xóa 1 học sinh ra khỏi 1 lớp
	Tham số vào: Mã học sinh, mã lớp.
	Trả về true nếu thêm thành công, ngược lại false và cho biết lỗi (nếu được).

 - Xoa1Lop
	Xóa thông tin 1 lớp
	Tham số vào: Mã lớp.
	Trả về true nếu thêm thành công, ngược lại false và cho biết lỗi (nếu được).	