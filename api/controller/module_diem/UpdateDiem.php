<?php
    require_once '../../model/model_diem/UpdateDiem.php';
    $data = (array) json_decode(file_get_contents('php://input'));
    
    //THAM SỐ CẦN THIẾT
    $mshs = $data['mshs'];//MÃ SỐ HỌC SINH
    $khoa = $data['khoa'];//KHÓA
    $mon = $data['mon'];//TÊN MÔN
    
    $hocKi = $data['hocki'];//HỌC KÌ 1 HOẶC 2
    $diem15 = $data['diem15'];//ĐIỂM UPDATE
    $diem1t = $data['diem1t'];//15P - 1T - HK
    $diemchk = $data['diemchk'];
    
    
    
    if(UpdateDiem($mshs, $khoa, $mon, $hocKi, $diem15, $diem1t, $diemchk)){
        echo '{"res":"true"}';
    }
    else{
        echo '{"res":"false"}';
    }