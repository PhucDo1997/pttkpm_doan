<?php
    require_once '../../model/model_diem/XoaDiem.php';
    $data = (array) json_decode(file_get_contents('php://input'));
    
    //THAM SỐ CẦN THIẾT
    $mshs = $data['mshs'];//MÃ SỐ HS
    $khoa = $data['khoa'];//KHÓA
    $loaiDiem = $data['loai'];//15P - 1T - CHK
    $hocki = $data['hocki'];//HỌC KÌ 1 HOẶC 2
    $mon = $data['mon'];//TÊN MÔN
    
    
    if(XoaDiem($mshs, $khoa, $mon, $hocki, $loaiDiem)){
        echo '{"res":"true"}';
    }
    else{
        echo '{"res":"false", "err":"xoa that bai"}';
    }