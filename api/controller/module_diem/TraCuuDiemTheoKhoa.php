<?php
    require_once '../../model/model_diem/TraCuuDiemTheoKhoa.php';
    $data = (array) json_decode(file_get_contents('php://input'));
    
    $mshs = $data['mshs'];//THAM SỐ CẦN THIẾT
    $khoa = $data['khoa'];//THAM SỐ CẦN THIẾT
    
    
    
    $result = TraCuuDiemTheoKhoa($mshs, $khoa);
    
    if($result == null){
        echo'{"res":"false", "err":"khong ton tai hoc sinh"}';
    }
    
    else{
        echo json_encode($result);
    }