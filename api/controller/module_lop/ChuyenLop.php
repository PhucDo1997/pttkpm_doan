<?php
    include_once '../../model/model_lop/ChuyenLop.php';
    $data = (array) json_decode(file_get_contents('php://input'));
    //THAM SỐ CẦN THIẾT
    $mshs = $data['mshs'];//MÃ SỐ HS
    $malopcu = $data['malopcu'];//MÃ LỚP CŨ
    $malopmoi = $data['malopmoi'];//MÃ LỚP MỚI
    
    $result = ChuyenLop($mshs, $malopcu, $malopmoi);
    
    if ($result == 'true'){
       echo '{"res":"true"}';
    }
    
    else {
       echo '{"res":"false", "err":"'. $result. '"}';
   }