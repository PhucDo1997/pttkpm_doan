<?php
    include_once '../../model/model_lop/LapDanhSachLop.php';
    $data = (array) json_decode(file_get_contents('php://input'));
    
    //THAM SỐ CẦN THIẾT
    $malop = $data['malop'];//THAM SỐ CẦN THIẾT
    $dsmahocsinh = $data['mshs'];//DS CÁC HS ĐƯỢC THÊM (ĐÃ CÓ TRONG CSDL)
    
    if(LapDanhSachLop($malop, $dsmahocsinh)){
        echo '{"res":"true"}';
    }
    else{
        echo '{"res":"false"}';
    }