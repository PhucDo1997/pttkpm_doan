<?php
    include_once '../../model/model_lop/ThemHSVaoLopCoSan.php';
    $data = (array) json_decode(file_get_contents('php://input'));
    
    //THAM SỐ CẦN THIẾT
    $malop = $data['malop'];//THAM SỐ CẦN THIẾT
    $dsmahocsinh = $data['dsmahs'];//DS CÁC HS ĐƯỢC THÊM (ĐÃ CÓ TRONG CSDL)
    
    if(ThemHSVaoLopCoSan($malop, $dsmahocsinh)){
        echo '{"res":"true"}';
    }
    else{
        echo '{"res":"false"}';
    }