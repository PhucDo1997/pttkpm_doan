<?php
$values = (array) json_decode(file_get_contents('php://input'));
include_once ('../../model/model_thongtin/BaoCaoTongKetHocKi.php');
$khoa = $values['KHOA'];
$hocki = $values['HOCKI'];
$baocao = BaoCaoTongKetHocKi($khoa, $hocki);
if(empty($baocao)){
    echo '{"res":"false"}';
}else{
    echo json_encode($baocao);
}