<?php
include_once ('../../model/model_thongtin/Add.php');
include_once ('../../model/model_thongtin/AddBangDiem.php');
include_once ('../../model/model_thongtin/SearchID.php');
$values = (array) json_decode(file_get_contents('php://input'));
$mshs = $values['MSHS'];
$ten = $values['HOTEN'];
$gioitinh = $values['GIOITINH'];
$ngaysinh = $values['NGAYSINH'];
$diachi = $values['DIACHI'];
$email = $values['EMAIL'];
if(!empty(SearchID($mshs))){
    echo '{"res":"false"}';
}else{
    if(Add($mshs, $ten, $gioitinh, $ngaysinh, $diachi, $email) == false){
        echo '{"res":"false"}';
    }else{
        if(AddBangDiem($mshs) == true){
            echo '{"res":"true"}';
        }else{
            echo '{"res":"false"}';
        }
    }
}