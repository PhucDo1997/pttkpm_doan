<?php
$values = (array) json_decode(file_get_contents('php://input'));
$user = $values['USERNAME'];
$pass = $values['PASSWORD'];
$type = $values['TYPE'];
include_once('../../libs/session.php');
if($type == 'admin'){
    include_once('../../model/model_thongtin/GetAccountGV.php');
    $giaovu = GetAccountGV($user, $pass);
    if(empty($giaovu)){
        echo '{"res":"false", "type":"admin"}';
    }else{
        setLogged($giaovu['TENTK'], "admin");
        echo '{"res":"true", "type":"admin"}';
    }
}else{
    include_once('../../model/model_thongtin/GetAccountHS.php');
    $hocsinh = GetAccountHS($user, $pass);
    if(empty($hocsinh)){
        echo '{"res":"false", "type":"student"}';
    }else{
        setLogged($hocsinh['MSHS'], "student");
        echo '{"res":"true", "type":"student"}';
    }
}