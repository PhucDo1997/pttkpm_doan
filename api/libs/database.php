<?php
$conn = null;

//kết nối database
function db_connect() {
    global $conn;
    if(!$conn) {
        header('Access-Control-Allow-Origin: *');
        $conn = mysqli_connect('localhost', 'root', '', 'QLHS') 
                or die ('coud not connect database');
        mysqli_set_charset($conn, 'UTF-8');
    }     
}

//ngắt kết nối
function db_close() {
    global $conn;
    if($conn) {
        mysqli_close($conn);
        $conn = null;
    }
}

//lấy data
function db_getList($sql) {
    db_connect();
    global $conn;
    $row = array();
    $result = mysqli_query($conn, $sql);
    
    if (!$result) {
        echo mysqli_error($conn);
    }
    //var_dump($result);
    $data = array();
    try {
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    } catch (Exception $ex) {
        return "";
    }
}

//lấy 1 record
function db_getRow($sql) {
    db_connect();
    global $conn;
    $result = mysqli_query($conn, $sql);
    $row = array();
    if (empty($result)) {
        echo NULL;
        return NULL;
    }
    if (mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
    }
    return $row;
}

//thực thi câu truy vấn insert, update, delete
function db_execute($sql) {
    db_connect();
    global $conn;
    return mysqli_query($conn, $sql);
}