<?php session_start();

//gán session
function session_set($key, $val) {
    $_SESSION[$key] = $val;
}

//lấy session
function session_get($key) {
    return (isset($_SESSION[$key])) ? $_SESSION[$key] : false;
}

//xóa session
function session_del($key) {
    if (isset($_SESSION[$key])) {
        unset($_SESSION[$key]);
    }
}