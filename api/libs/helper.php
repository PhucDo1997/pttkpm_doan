<?php
// Hàm tạo URL
function baseUrl($uri = ''){
    return 'http://localhost/src/'.$uri;
}
 
// Hàm redirect
function redirect($url){
    header("Location:{$url}");
    exit();
}
 
// Hàm lấy value từ $_POST
function inputPost($key){
    return isset($_POST[$key]) ? trim($_POST[$key]) : false;
}
 
// Hàm lấy value từ $_GET
function inputGet($key){
    return isset($_GET[$key]) ? trim($_GET[$key]) : false;
}

function isSubmit($key, $val){
    return (isset($_POST[$key]) && $_POST[$key] == $val);
}