<?php
function BaoCaoTongKetHocKi($khoa, $hocki){
    include_once ('../../libs/database.php');
    $sqlSearch = "select l.MALOP as 'MALOP', l.KHOI as 'KHOI', l.TEN as 'TEN', l.SISO as 'SISO', count(H.ms) as 'SLD', (count(H.ms) / l.SISO)*100 as 'TYLE'
                    from lop l left join (select hs.MSHS as 'ms', hs.MALOP as 'malop'
                                            from hocsinh hs, diem15p d15p, diem1t d1t, diemchk dchk
                                            where hs.MSHS=d15p.MSHS 
                                            and hs.MSHS=d1t.MSHS 
                                            and hs.MSHS=dchk.MSHS 
                                            and ((d15p.HK" . $hocki . "TOAN + d1t.HK" . $hocki . "TOAN + dchk.HK" . $hocki . "TOAN)/3)>=5 
                                            and ((d15p.HK" . $hocki . "LY + d1t.HK" . $hocki . "LY + dchk.HK" . $hocki . "LY)/3)>=5 
                                            and ((d15p.HK" . $hocki . "HOA + d1t.HK" . $hocki . "HOA + dchk.HK" . $hocki . "HOA)/3)>=5 
                                            and d15p.KHOA = '$khoa' 
                                            and d1t.KHOA = '$khoa' 
                                            and dchk.KHOA = '$khoa') H
                on l.MALOP = H.malop
                group by l.MALOP, H.malop;";
    $lop = db_getList($sqlSearch);
    db_close();
    if(!empty($lop)){ return $lop;
    }
    return null;
}